package Exceptions
import (
	"fmt"
	"path"
	"runtime"
	"github.com/jinzhu/gorm"
)



// EXCEPTIONS MECHANISM

func Throw(v interface{}) {
	_, file, line, _ := runtime.Caller(1)
	_, file = path.Split(file)
	panic(fmt.Errorf("%s:%d: %v", file, line, v))
}

func _throw(s string) {
	_, file, line, _ := runtime.Caller(2)
	_, file = path.Split(file)
	panic(fmt.Errorf("%s:%d: %s", file, line, s))
}


func Catch(handler func(r interface{})) {
	if r := recover(); r != nil {
		handler(r)
	}
}

func B(b bool, v ...interface{}) {
	if b {
		_throw(fmt.Sprint(v))
	}
}

func E(err error) {
	if err != nil {
		_throw(fmt.Sprintf("%s", err.Error()))
	}
}

func ES(err error, s string) {
	if err != nil {
		_throw(fmt.Sprintf("%s (%s)", s, err.Error()))
	}
}

func BSF(b bool, s string, f func()) {
	if b {
		f()
		_throw(s)
	}
}

func ESF(err error, s string, f func()) {
	if err != nil {
		f()
		_throw(fmt.Sprintf("%s (%s)", s, err.Error()))
	}
}

func FFS(f func(), h func(), s string) {
	defer func() {
		if r := recover(); r != nil {
			h()
			_throw(fmt.Sprintf("%+v ( %+v )", s, r))
		}
	}()
	f()
}

func Onpanic(f func()) {
	if r := recover(); r != nil {
		f()
		panic(r)
	}
}

func D(db *gorm.DB) {
	if err := db.Error; err != nil {
		_throw(fmt.Sprintf("%s", err.Error()))
	}
}

func DO(db *gorm.DB) {
	if err := db.Error; err != nil && err != gorm.RecordNotFound {
		_throw(fmt.Sprintf("%s", err.Error()))
	}
}

func DF(db *gorm.DB, f func()) {
	if err := db.Error; err != nil {
		f()
		_throw(fmt.Sprintf("%s", err.Error()))
	}
}

func DS(db *gorm.DB, errorString string) {
	if err := db.Error; err != nil {
		_throw(fmt.Sprintf("%s (%s)", errorString, err.Error()))
	}
}


func DFS(db *gorm.DB, f func(), errorString string) {
	if err := db.Error; err != nil {
		f()
		_throw(fmt.Sprintf("%s (%s)", errorString, err.Error()))
	}
}


func DNES(db *gorm.DB, errorString string) {
	if err := db.Error; err == nil {
		_throw(errorString)
	}
}
