package main
import (
	E "Exceptions"
	"sync"
	"strings"
	"fmt"
	"os"
	"time"
	"github.com/franela/goreq"
	"github.com/jessevdk/go-flags"

	"github.com/jinzhu/gorm"
	_ "github.com/go-sql-driver/mysql"
	"log"
	_ "net/http/pprof"
	"net/http"
	"regexp"
	"bufio"
)

type Article struct {
	gorm.Model
	Title      string
	Text       string
	URL        string

	Categories []Category `gorm:"many2many:categories_articles"`
}

type Category struct {
	gorm.Model
	Title string
}

type MatchError struct {
	Article    Article
	Expression string
}
func (a MatchError)Error() string {
	return "Article " + a.Article.Title + " failed matching regexp " + a.Expression
}

type WikiParams struct {
	Format      string `url:"format,omitempty"`
	Action      string `url:"action,omitempty"`
	List        string `url:"list,omitempty"`
	Prop        string `url:"prop,omitempty"`
	ExPlainText bool `url:"explaintext,omitempty"`
	SrSearch    string `url:"srsearch,omitempty"`
	Continue    string `url:"continue,omitempty"`
	SrOffset    string `url:"sroffset,omitempty"`
	Titles      string `url:"titles,omitempty"`
	InProp      string `url:"inprop,omitempty"`
}

var AppArgs struct {
	SearchQuery   string `long:"search" short:"s" required:"true"  description:"Search query. Don't forget the quote marks!!"`
	RegExp        string `long:"regex" short:"r" default:"факт" description:"Regex to be applied. Last match group is used. PCRE syntax. Flags are /mi/."`
	TimeoutWait   uint `long:"timeout" short:"t" default:"60" description:"Seconds to halt after getting rejected by wiki"`
	OutFilePath   string `long:"out" short:"o" description:"Output file path. If unspecified - stdout."`
	ErrFilePath   string `long:"err" short:"e" description:"Error file path. If unspecified - stderr."`
	SearchGophers uint `long:"searchGophers" default:"3" description:"Number of gophers involved in quering wikipedia for articles listings"`
	FetchGophers  uint `long:"fetchGophers" default:"30" description:"Number of gophers fetching articles and parsing jsons from wiki"`
	RegexpGophers uint `long:"regexpGophers" default:"10" description:"Number of gophers applying regexes to articles"`
}



var outputWaitGroup sync.WaitGroup
var timeoutWaitGroup sync.WaitGroup

func main() {
	if _, err := flags.Parse(&AppArgs); err != nil {
		log.Fatal(err)
	}
	go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()

	fmt.Println("Tearing appart wikipedia in search of \"" + AppArgs.SearchQuery + "\"")

	search, searchErrChan := GoArticleFinders(AppArgs.SearchGophers)
	articles, articlesErrChan := GoArticleFetchers(AppArgs.FetchGophers, search)
	filtered, filteredErrChan := GoArticleFilters(AppArgs.RegexpGophers, articles)
	resC1, resC2 := multiplex(filtered)

	out := setupOutput()
	fileDumperErrChan := GoFileDumper(out.output, resC1)
	dbDumperErrChan := GoDBDumper(out.db, resC2)
	GoErrorDumper(out.errorOutput, mergeErrorChans(searchErrChan, articlesErrChan, filteredErrChan, fileDumperErrChan, dbDumperErrChan))

	outputWaitGroup.Wait()
}

func GoArticleFinders(count uint) (chan Article, <-chan error) {
	var wg sync.WaitGroup
	c := make(chan Article, 10000)
	e := make(chan error)

	finished := false
	g := make(chan int)
	go func() {
		defer close(g)
		for i := 0; finished == false; i = i + 10 {
			g <- i
		}
	}()

	var work func()
	start := func() {
		wg.Add(1)
		go work()
	}

	work = func() {
		var offset int
		defer wg.Done()
		defer catchError("[Finder]", e, start, func() {
			timeoutWaitGroup.Add(1)
			time.Sleep(time.Duration(AppArgs.TimeoutWait) * time.Second)
			timeoutWaitGroup.Add(-1)
			//			g <- offset
		})

		for offset = range g {
			timeoutWaitGroup.Wait()
			r, err := goreq.Request{
				Uri:"https://ru.wikipedia.org/w/api.php",
				QueryString:WikiParams{
					Format:"json",
					Action:"query",
					List:"search",
					SrSearch:AppArgs.SearchQuery,
					Continue:"-||",
					SrOffset:fmt.Sprint(offset),
				},
				Accept: "application/json",
				UserAgent: "goreq",
				Timeout:10 * time.Second,
			}.Do()
			E.E(err)

			var tmp struct {
				Query struct {
						  Search []struct {
							  Title string
						  }
					  }
			}
			err = r.Body.FromJsonTo(&tmp)
			r.Body.Close()
			E.E(err)

			if len(tmp.Query.Search) == 0 {
				finished = true
			}

			for _, page := range tmp.Query.Search {
				E.B(len(page.Title) == 0, "Article has no title")
				c <- Article{
					Title:page.Title,
				}
			}
		}
	}

	for i := uint(0); i < count; i++ {
		start()
	}

	go func() {
		wg.Wait()
		close(c)
		close(e)
	}()

	return c, e
}

func GoArticleFetchers(count uint, input chan Article) (<-chan Article, <-chan error) {
	var wg sync.WaitGroup
	c := make(chan Article, 10000)
	e := make(chan error)

	var work func()
	start := func() {
		wg.Add(1)
		go work()
	}

	work = func() {
		var article Article
		defer wg.Done()
		defer catchError("[Fetcher]", e, start, func() {
			timeoutWaitGroup.Add(1)
			time.Sleep(time.Duration(AppArgs.TimeoutWait) * time.Second)
			timeoutWaitGroup.Add(-1)
			//			input <- article
		})

		for article = range input {
			timeoutWaitGroup.Wait()
			r, err := goreq.Request{
				Uri:"https://ru.wikipedia.org/w/api.php",
				QueryString:WikiParams{
					Format:"json",
					Action:"query",
					Prop:"extracts|info|categories",
					InProp:"url",
					ExPlainText:true,
					Titles:article.Title,
				},
				Accept: "application/json",
				UserAgent: "goreq",
				Timeout:10 * time.Second,
			}.Do()
			E.E(err)

			var tmp struct {
				Query struct {
						  Pages map[string]struct {
							  Title        string
							  Extract      string
							  Canonicalurl string
							  Categories   []struct {
								  Title string
							  }
						  }
					  }
			}
			err = r.Body.FromJsonTo(&tmp)
			r.Body.Close()
			E.E(err)

			for _, page := range tmp.Query.Pages {
				E.B(len(page.Title) == 0, "Article has no title")
				E.B(len(page.Extract) == 0, "Article has no content")
				E.B(len(page.Canonicalurl) == 0, "Article has no URL")
				E.B(page.Title != article.Title, "Article name mismatch", page.Title, article.Title)

				article.Text = page.Extract
				article.URL = page.Canonicalurl
				for _, cs := range page.Categories {
					article.Categories = append(article.Categories, Category{Title:cs.Title[len("Категория:"):]})
				}

				c <- article
			}
		}
	}

	for i := uint(0); i < count; i++ {
		start()
	}

	go func() {
		wg.Wait()
		close(c)
		close(e)
	}()

	return c, e
}

func GoArticleFilters(count uint, input <-chan Article) (<-chan Article, <-chan error) {
	var wg sync.WaitGroup
	c := make(chan Article)
	e := make(chan error)

	r := regexp.MustCompile("(?im)^(=+).*?"+AppArgs.RegExp+".*?(=*)$")

	var start func()
	work := func() {
		defer wg.Done()
		defer catchError("[Filter]", e, start, func() {})

		for article := range input {
			indexies := r.FindStringSubmatchIndex(article.Text)
			E.B(indexies == nil, article.Title + " does not match regex")

			scanner := bufio.NewScanner(strings.NewReader(article.Text[indexies[0]:]))
			scanner.Split(bufio.ScanLines)

			resultSlice := []string{}
			equalSigns := 0
			for scanner.Scan() {
				text := scanner.Text()
				if equalSigns == 0 {
					equalSigns = strings.Count(text, "=") / 2
				} else if strings.HasPrefix(text, "=") && strings.Count(text, "=") / 2 <= equalSigns {
					break
				}
				resultSlice = append(resultSlice, text)
			}
			article.Text = strings.Join(resultSlice, "\n")

			c <- article
		}
	}

	start = func() {
		wg.Add(1)
		go work()
	}

	for i := uint(0); i < count; i++ {
		start()
	}

	go func() {
		wg.Wait()
		close(c)
		close(e)
	}()

	return c, e
}

func GoFileDumper(out *os.File, c <-chan Article) <-chan error {
	errorChan := make(chan error)

	var start func()
	work := func() {
		defer outputWaitGroup.Done()
		defer catchError("[FileDumper]", errorChan, start, func() {  })

		count := 0
		start := time.Now()

		for f := range c {
			count++

			_, err := os.Stdout.WriteString("\x1b[2K\r")
			E.E(err)

			categories := []string{}
			for _, c := range f.Categories {
				categories = append(categories, c.Title)
			}
			_, err = out.WriteString(fmt.Sprintf("Статья: %s\nКатегории: %s\nСсылка: %s\n%s\n================\n", f.Title, strings.Join(categories, ", "), f.URL, f.Text))
			E.E(err)
			_, err = os.Stdout.WriteString(fmt.Sprintf("%d %.02fa/s %s", count, float32(count) / (float32(time.Since(start)) / float32(time.Second)), f.Title))
			E.E(err)
		}
		close(errorChan)
	}

	start = func() {
		outputWaitGroup.Add(1)
		go work()
	}

	start()

	return errorChan
}

func GoDBDumper(db *gorm.DB, c <-chan Article) <-chan error {
	errorChan := make(chan error)
	var start func()

	work := func() {
		defer outputWaitGroup.Done()
		defer catchError("[DBDumper]", errorChan, start, func() {  })

		for f := range c {
			for _, category := range f.Categories {
				E.D(db.FirstOrCreate(&category))
			}
			E.D(db.Assign(&f).FirstOrCreate(&Article{}, &Article{Title:f.Title}))
		}
		close(errorChan)
	}

	start = func() {
		outputWaitGroup.Add(1)
		go work()
	}

	start()
	return errorChan
}

func GoErrorDumper(out *os.File, c <-chan error) {
	outputWaitGroup.Add(1)

	go func() {
		defer outputWaitGroup.Done()
		for e := range c {
			_, err := out.WriteString(e.Error() + "\n")
			E.E(err)
		}
	}()
}

func setupOutput() (out struct {db *gorm.DB; output *os.File; errorOutput *os.File}) {
	out.db = connectDB()

	if len(AppArgs.OutFilePath) != 0 {
		file, err := os.Create(AppArgs.OutFilePath)
		E.E(err)
		out.output = file
	} else {
		out.output = os.Stdout
	}

	if len(AppArgs.ErrFilePath) != 0 {
		file, err := os.Create(AppArgs.ErrFilePath)
		E.E(err)
		out.errorOutput = file
	} else {
		out.errorOutput = os.Stderr
	}

	return
}

func catchError(sender string, e chan <- error, restart func(), retry func()) {
	if r := recover(); r != nil {
		switch r.(type) {
		case error:
			str := strings.ToLower(r.(error).Error())
			if strings.Contains(str, "timeout") || strings.Contains(str, "cancel") {
				e <- fmt.Errorf("%s Timeout. Will retry.", sender)
				retry()
			} else {
				e <- fmt.Errorf("%s Error: %s", sender, r.(error).Error())
			}
		default:
			e <- fmt.Errorf("%s Catched unknown panic!", sender)
		}
		restart()
	}
}

func multiplex(c <-chan Article) (<-chan Article, <-chan Article) {
	c1 := make(chan Article)
	c2 := make(chan Article)
	go func() {
		defer close(c1)
		defer close(c2)

		for article := range c {
			c1 <- article
			c2 <- article
		}
	}()
	return c1, c2
}

func mergeErrorChans(cs ...<-chan error) <-chan error {
	var wg sync.WaitGroup
	out := make(chan error)

	output := func(c <-chan error) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}

	for _, c := range cs {
		go output(c)
		wg.Add(1)
	}

	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

func connectDB() *gorm.DB {
	db, err := gorm.Open("mysql", "aspcartman:vicevice@/WikiFetcher?charset=utf8&parseTime=True&loc=Local")
	E.E(err)

	db.DB().Ping()
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)


	E.D(db.AutoMigrate(&Article{}, &Category{}))

	return &db
}